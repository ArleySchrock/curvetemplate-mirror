CurveTemplate
=============

ASP.NET MVC 4 template using a modern CSS layout and jQuery slider

The theme for this was taken from http://chocotemplates.com/corporate/curve/ and modified for use in the ASP.NET environment with the Razor view engine
